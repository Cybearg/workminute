package com.cybearg.workminute;

import android.app.Fragment;
import android.app.ListFragment;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.AbstractMap;


public class TaskListActivity extends FragmentActivity
        implements TaskListFragment.Callbacks {

    private static final int request_code = 1;

    private long _taskID;
    private long _parentID;
    private boolean _isQueue;

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    public final static String TASK_ID = "com.cybearg.workminute.TASK_ID";
    public final static String PARENT_ID = "com.cybearg.workminute.PARENT_ID";
    public final static String IS_QUEUE = "com.cybearg.workminute.IS_QUEUE";

    public void btnAddTask_Click(View view) {
        Intent intent = new Intent(this, CreateEditTaskActivity.class);
        intent.putExtra(TASK_ID, -1);
        intent.putExtra(PARENT_ID, _taskID);
        startActivityForResult(intent, request_code);
    }

    public void btnEditTask_Click(View view) {
        Intent intent = new Intent(this, CreateEditTaskActivity.class);
        intent.putExtra(TASK_ID, _taskID);
        intent.putExtra(PARENT_ID, -1);
        startActivityForResult(intent, request_code);
    }

    public void btnAddNote_Click(View view) {
        // No way to add notes for the time being
    }

    public void btnBack_Click(View view) {
        Intent data = new Intent();
        data.putExtra(TASK_ID, _parentID);
        setResult(RESULT_OK, data);
        this.finish();
    }

    public void btnViewQueue_Click(View view) {
        Intent intent = new Intent(this, TaskListActivity.class);
        intent.putExtra(TASK_ID, _taskID);
        intent.putExtra(PARENT_ID, _parentID);
        intent.putExtra(IS_QUEUE, true);
        startActivityForResult(intent, request_code);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == request_code) {
            TaskListFragment fragment = (TaskListFragment) getSupportFragmentManager().findFragmentById(R.id.task_list);
            if (resultCode == RESULT_OK) {
                if (!_isQueue)
                {
                    if (_taskID == -1)
                        fragment.updateTaskList();
                    else
                    {
                        fragment.updateTaskList(_taskID);
                        updateTaskInfo();
                    }
                }
                else
                {
                    fragment.updateTaskQueue(_taskID);
                }
            } else if (resultCode == -2) {
                DBHelper dbHelper = new DBHelper(ApplicationContextProvider.getContext());
                if (_isQueue && (_taskID == -1 || dbHelper.recursiveGetChildTasks(_taskID).size() > 0))
                    fragment.updateTaskQueue(_taskID);
                else if (_taskID == -1)
                {
                    fragment.updateTaskList();
                }
                else
                {
                    setResult(RESULT_OK, data);
                    this.finish();
                }
            }

        }
    }

    private void updateTaskInfo() {
        DBHelper dbHelper = new DBHelper(ApplicationContextProvider.getContext());
        Task task = dbHelper.getTask(_taskID);
        TextView txtTaskName = (TextView) findViewById(R.id.txtTaskName);
        txtTaskName.setText(task.getTaskName());

        // Below is copied from TaskAdapter class
        TextView taskPriority = (TextView) findViewById(R.id.taskPriority);
        TextView taskImportance = (TextView) findViewById(R.id.taskImportance);
        int noteCount;
        int childCount;

        TextView taskNotes = (TextView) findViewById(R.id.taskNotes);
        TextView taskChildren = (TextView) findViewById(R.id.taskChildren);

        // Run queries to get counts of notes and children
        noteCount = dbHelper.getTaskNotes(task.getTaskId()).size();
        childCount = dbHelper.recursiveGetChildTasks(task.getTaskId()).size();

        taskNotes.setText(Integer.toString(noteCount) + " " + getString(R.string.task_notecount_label));
        taskNotes.setVisibility(View.GONE);
        taskChildren.setText(Integer.toString(childCount) + " " + getString(R.string.task_childcount_label));

        // For assigning colors/names to priority and importance text
        AbstractMap.SimpleEntry<Integer, String> colorNamePair;

        colorNamePair = TaskAdapter.findUrgency(task.getPriority());
        taskPriority.setText(colorNamePair.getValue());
        taskPriority.setTextColor(colorNamePair.getKey());

        colorNamePair = TaskAdapter.findUrgency(task.getImportance());
        taskImportance.setText(colorNamePair.getValue());
        taskImportance.setTextColor(colorNamePair.getKey());

        TaskListFragment fragment = (TaskListFragment) getSupportFragmentManager().findFragmentById(R.id.task_list);

        Button btnViewQueue = (Button) findViewById(R.id.btnViewQueue);
        btnViewQueue.setEnabled(fragment.taskCount() > 0);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_list);

        TaskListFragment fragment = (TaskListFragment) getSupportFragmentManager().findFragmentById(R.id.task_list);

        Intent intent = getIntent();
        _taskID = intent.getLongExtra(TASK_ID, -1);
        _parentID = intent.getLongExtra(PARENT_ID, -1);
        _isQueue = intent.getBooleanExtra(IS_QUEUE, false);

        fragment.setTask(_taskID, _isQueue);

        TextView txtTaskName = (TextView) findViewById(R.id.txtTaskName);
        Button btnAddNote = (Button) findViewById(R.id.btnAddNote);
        btnAddNote.setVisibility(View.GONE);
        btnAddNote.setEnabled(false);

        if (_isQueue)
        {
            Button btnNewTask = (Button) findViewById(R.id.btnNewTask);
            Button btnEditTask = (Button) findViewById(R.id.btnEditTask);
            Button btnViewQueue = (Button) findViewById(R.id.btnViewQueue);
            LinearLayout llTaskInfo = (LinearLayout) findViewById(R.id.llTaskInfo);

            btnNewTask.setVisibility(View.GONE);
            btnEditTask.setVisibility(View.GONE);
            btnViewQueue.setVisibility(View.GONE);
            llTaskInfo.setVisibility(View.GONE);

            txtTaskName.setText("Task Queue");
        }
        else if (_taskID == -1)
        {
            Button btnEditTask = (Button) findViewById(R.id.btnEditTask);
            Button btnBack = (Button) findViewById(R.id.btnBack);
            LinearLayout llTaskInfo = (LinearLayout) findViewById(R.id.llTaskInfo);

            btnEditTask.setEnabled(false);
            btnBack.setEnabled(false);
            llTaskInfo.setVisibility(View.GONE);
            txtTaskName.setText(getString(R.string.label_all_projects));
        }
        else
        {
            updateTaskInfo();
        }

        /*
        Bundle arguments = new Bundle();
        arguments.putLong(TaskListActivity.TASK_ID, _taskID);
        arguments.putLong(TaskListActivity.PARENT_ID, _parentID);
        fragment.setArguments(arguments);
        */

        View view = this.getWindow().getDecorView();
        view.setBackgroundColor(Color.parseColor("#454545"));
    }

    /**
     * Callback method from {@link TaskListFragment.Callbacks}
     * indicating that the item with the given ID was selected.
     */
    @Override
    public void onItemSelected(String id) {
        // In single-pane mode, simply start the detail activity
        // for the selected item ID.
        //Intent detailIntent = new Intent(this, TaskDetailActivity.class);
        //detailIntent.putExtra(TaskDetailFragment.ARG_ITEM_ID, id);
        //startActivity(detailIntent);
        if (_isQueue)
        {
            Intent intent = new Intent(this, CreateEditTaskActivity.class);
            intent.putExtra(TASK_ID, Long.valueOf(id));
            intent.putExtra(PARENT_ID, -1);
            startActivityForResult(intent, request_code);
        }
         else
        {
            Intent intent = new Intent(this, TaskListActivity.class);
            intent.putExtra(TASK_ID, Long.valueOf(id));
            intent.putExtra(PARENT_ID, _taskID);
            intent.putExtra(IS_QUEUE, _isQueue);
            startActivityForResult(intent, request_code);
        }
    }
}
