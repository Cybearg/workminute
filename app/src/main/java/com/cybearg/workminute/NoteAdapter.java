package com.cybearg.workminute;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Cybearg on 4/18/2015.
 */
public class NoteAdapter extends ArrayAdapter<Note> {
    private ArrayList<Note> notes;

    public NoteAdapter(Context context, ArrayList<Note> notes) {
        super(context, 0, notes);
        this.notes = notes;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Note note = notes.get(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_note, parent, false);
        }

        if (note != null) {
            TextView noteName = (TextView) convertView.findViewById(R.id.noteName);
            TextView noteText = (TextView) convertView.findViewById(R.id.noteText);
            TextView noteID = (TextView) convertView.findViewById(R.id.noteID);

            noteName.setText(note.getName());
            noteText.setText(note.getText());
            noteID.setText(Long.toString(note.getNoteId()));
        }

        return convertView;
    }
}
