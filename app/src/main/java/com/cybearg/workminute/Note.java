package com.cybearg.workminute;

import java.sql.Date;
import java.util.Calendar;

/**
 * Created by Cybearg on 4/9/2015.
 */
public class Note {

    // Table name
    public static final String TABLE = "notes";

    // Column names
    public static final String NOTE_ID = "note_id";
    public static final String NOTE_NAME = "note_name";
    public static final String NOTE_TEXT = "note_text";
    public static final String NOTE_COLOR = "note_color";
    public static final String NOTE_CREATED = "note_created";
    public static final String NOTE_LAST_MODIFIED = "note_last_modified";

    private long _note_id;
    private int _color;
    private String _name, _text;
    private Date _created, _last_modified;

    public Note()
    {
        this._note_id = 0;
        this._name = "";
        this._text = "";
        this._color = 0;
        Calendar cal = Calendar.getInstance();
        java.util.Date currDate = cal.getTime();
        this._created = new java.sql.Date(currDate.getTime());
        this._last_modified = new java.sql.Date(currDate.getTime());
    }

    public Note(int note_id, String name, String text, int color, Date created, Date last_modified)
    {
        this._note_id = note_id;
        this._name = name;
        this._text = text;
        this._color = color;
        this._created = created;
        this._last_modified = last_modified;
    }

    public long getNoteId() {
        return _note_id;
    }

    public void setNoteId(long _note_id) {
        this._note_id = _note_id;
    }

    public int getColor() {
        return _color;
    }

    public void setColor(int _color) {
        this._color = _color;
    }

    public String getName() { return _name; }

    public void setName(String _name) { this._name = _name; }

    public String getText() {
        return _text;
    }

    public void setText(String _text) {
        this._text = _text;
    }

    public Date getCreated() {
        return _created;
    }

    public void setCreated(Date _created) {
        this._created = _created;
    }

    public Date getLastModified() {
        return _last_modified;
    }

    public void setLastModified(Date _last_modified) {
        this._last_modified = _last_modified;
    }
}
