package com.cybearg.workminute;

/**
 * Created by Cybearg on 4/9/2015.
 */
public class Tag {
    // Table name
    public static final String TABLE = "tags";

    // Column names
    public static final String TAG_ID = "tag_id";
    public static final String TAG_NAME = "tag_name";
    public static final String TAG_COLOR = "tag_color";

    private long _tag_id;
    private int _color;
    private String _name;

    public Tag()
    {
        this._tag_id = 0;
        this._name = "";
        this._color = 0;
    }

    public Tag(int tag_id, String tag_name, int tag_color)
    {
        this._tag_id = tag_id;
        this._name = tag_name;
        this._color = tag_color;
    }

    public long getTagId() {
        return _tag_id;
    }

    public void setTagId(long _tag_id) {
        this._tag_id = _tag_id;
    }

    public int getColor() {
        return _color;
    }

    public void setColor(int _color) {
        this._color = _color;
    }

    public String getTagName() {
        return _name;
    }

    public void setTagName(String _name) {
        this._name = _name;
    }
}
