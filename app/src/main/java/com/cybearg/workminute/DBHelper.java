package com.cybearg.workminute;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

/**
 * Created by Cybearg on 4/7/2015.
 */
public class DBHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "workminute.db";

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_TABLE_TASKS = "CREATE TABLE " + Task.TABLE + "("
                + Task.TASK_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + Task.PARENT_ID + " INTEGER DEFAULT NULL, "
                + Task.TASK_NAME + " TEXT DEFAULT 'New Task', "
                + Task.TASK_DESCRIPTION + " TEXT, "
                + Task.TASK_COLOR + " INTEGER DEFAULT 0, "
                + Task.TASK_PROGRESS + " INTEGER DEFAULT 0, "
                + Task.TASK_IMPORTANCE + " INTEGER DEFAULT 0, "
                + Task.TASK_PRIORITY + " INTEGER DEFAULT 0, "
                + Task.TASK_TIME_ESTIMATE + " INTEGER DEFAULT 0, "
                + Task.TASK_TIME_ELAPSED + " INTEGER DEFAULT 0, "
                + Task.TASK_CREATED + " INTEGER DEFAULT 0, "
                + Task.TASK_LAST_MODIFIED + " INTEGER DEFAULT 0, "
                + Task.TASK_DEADLINE + " INTEGER DEFAULT 0 )";

        String CREATE_TABLE_TAGS = "CREATE TABLE " + Tag.TABLE + "("
                + Tag.TAG_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + Tag.TAG_NAME + " TEXT, "
                + Tag.TAG_COLOR + " INTEGER DEFAULT 0 )";

        String CREATE_TABLE_NOTES = "CREATE TABLE " + Note.TABLE + "("
                + Note.NOTE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + Note.NOTE_NAME + " TEXT, "
                + Note.NOTE_TEXT + " TEXT, "
                + Note.NOTE_COLOR + " INTEGER DEFAULT 0, "
                + Note.NOTE_CREATED + " NUMERIC, "
                + Note.NOTE_LAST_MODIFIED + " NUMERIC )";

        String CREATE_TABLE_TASK_NOTE_MAPPING = "CREATE TABLE " + TaskNoteMapping.TABLE + "("
                + TaskNoteMapping.TASK_ID + " INTEGER NOT NULL, "
                + TaskNoteMapping.NOTE_ID + " INTEGER NOT NULL )";

        String CREATE_TABLE_TASK_TAG_MAPPING = "CREATE TABLE " + TaskTagMapping.TABLE + "("
                + TaskTagMapping.TASK_ID + " INTEGER NOT NULL, "
                + TaskTagMapping.TAG_ID + " INTEGER NOT NULL )";

        db.execSQL(CREATE_TABLE_TASKS);
        db.execSQL(CREATE_TABLE_TAGS);
        db.execSQL(CREATE_TABLE_NOTES);
        db.execSQL(CREATE_TABLE_TASK_NOTE_MAPPING);
        db.execSQL(CREATE_TABLE_TASK_TAG_MAPPING);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + Task.TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + Tag.TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + Note.TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + TaskNoteMapping.TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + TaskTagMapping.TABLE);
        onCreate(db);
    }

    //// CRUD METHODS ////

      ///////////////////////
     ////// Task CRUD //////
    ///////////////////////
    public long addTask(Task task)
    {
        ContentValues values = new ContentValues();
        values.put(Task.PARENT_ID, task.getParentId());
        values.put(Task.TASK_NAME, task.getTaskName());
        values.put(Task.TASK_DESCRIPTION, task.getDescription());
        values.put(Task.TASK_COLOR, task.getColor());
        values.put(Task.TASK_PROGRESS, task.getProgress());
        values.put(Task.TASK_IMPORTANCE, task.getImportance());
        values.put(Task.TASK_PRIORITY, task.getPriority());
        values.put(Task.TASK_TIME_ESTIMATE, task.getEstimate());
        values.put(Task.TASK_TIME_ELAPSED, task.getElapsed());
        values.put(Task.TASK_CREATED, task.getCreated().getTime());
        values.put(Task.TASK_LAST_MODIFIED, task.getLastModified().getTime());
        values.put(Task.TASK_DEADLINE, task.getDeadline().getTime());

        SQLiteDatabase db = this.getWritableDatabase();

        long task_id = db.insert(Task.TABLE, null, values);
        db.close();
        return task_id;
    }

    public void updateTask(Task task)
    {
        updateTask(task, Task.TASK_ID + "=?", new String[] { String.valueOf(task.getTaskId()) });
    }

    public void updateTask(Task task, String where, String[] queryValues)
    {
        ContentValues values = new ContentValues();
        values.put(Task.TASK_ID, task.getTaskId());
        values.put(Task.PARENT_ID, task.getParentId());
        values.put(Task.TASK_NAME, task.getTaskName());
        values.put(Task.TASK_DESCRIPTION, task.getDescription());
        values.put(Task.TASK_COLOR, task.getColor());
        values.put(Task.TASK_PROGRESS, task.getProgress());
        values.put(Task.TASK_IMPORTANCE, task.getImportance());
        values.put(Task.TASK_PRIORITY, task.getPriority());
        values.put(Task.TASK_TIME_ESTIMATE, task.getEstimate());
        values.put(Task.TASK_TIME_ELAPSED, task.getElapsed());
        values.put(Task.TASK_CREATED, task.getCreated().toString());
        values.put(Task.TASK_LAST_MODIFIED, task.getLastModified().toString());
        values.put(Task.TASK_DEADLINE, task.getDeadline().toString());

        SQLiteDatabase db = this.getWritableDatabase();

        db.update(Task.TABLE, values, where, queryValues);
        db.close();
    }

    public ArrayList<Task> getTasks() {
        return getTasks("1=1", null);
    }

    public ArrayList<Task> getParentTasks() { return getTasks(Task.PARENT_ID + "=?", new String[] {"-1"}); }

    public ArrayList<Task> getTasks(String where, String[] queryValues) {
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT "
                + Task.TASK_ID + ", "
                + Task.PARENT_ID + ", "
                + Task.TASK_NAME + ", "
                + Task.TASK_DESCRIPTION + ", "
                + Task.TASK_COLOR + ", "
                + Task.TASK_PROGRESS + ", "
                + Task.TASK_IMPORTANCE + ", "
                + Task.TASK_PRIORITY + ", "
                + Task.TASK_TIME_ESTIMATE + ", "
                + Task.TASK_TIME_ELAPSED + ", "
                + Task.TASK_CREATED + ", "
                + Task.TASK_LAST_MODIFIED + ", "
                + Task.TASK_DEADLINE +
                " FROM " + Task.TABLE +
                " WHERE " + where;
        ArrayList<Task> taskList = new ArrayList<Task>();

        Task task;
        Cursor cursor = db.rawQuery(selectQuery, queryValues);

        if (cursor.moveToFirst()) {
            do {
                task = new Task();

                task.setTaskId(cursor.getLong(cursor.getColumnIndex(Task.TASK_ID)));
                task.setParentId(cursor.getLong(cursor.getColumnIndex(Task.PARENT_ID)));
                task.setTaskName(cursor.getString(cursor.getColumnIndex(Task.TASK_NAME)));
                task.setDescription(cursor.getString(cursor.getColumnIndex(Task.TASK_DESCRIPTION)));
                task.setColor(cursor.getInt(cursor.getColumnIndex(Task.TASK_COLOR)));
                task.setProgress((byte) cursor.getInt(cursor.getColumnIndex(Task.TASK_PROGRESS)));
                task.setImportance(cursor.getInt(cursor.getColumnIndex(Task.TASK_IMPORTANCE)));
                task.setPriority(cursor.getInt(cursor.getColumnIndex(Task.TASK_PRIORITY)));
                task.setEstimate(cursor.getInt(cursor.getColumnIndex(Task.TASK_TIME_ESTIMATE)));
                task.setElapsed(cursor.getInt(cursor.getColumnIndex(Task.TASK_TIME_ELAPSED)));
                task.setCreated(new java.sql.Date(cursor.getLong(cursor.getColumnIndex(Task.TASK_CREATED))));
                task.setLastModified(new java.sql.Date(cursor.getLong(cursor.getColumnIndex(Task.TASK_LAST_MODIFIED))));
                task.setDeadline(new java.sql.Date(cursor.getLong(cursor.getColumnIndex(Task.TASK_DEADLINE))));

                taskList.add(task);
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return taskList;
    }

    public ArrayList<Task> recursiveGetChildTasks(long task_id) {
        ArrayList<Task> childList = getChildTasks(task_id);
        ArrayList<Task> returnList = new ArrayList<Task>();

        returnList.addAll(childList);
        for (Task t : childList)
            returnList.addAll(recursiveGetChildTasks(t.getTaskId()));

        return returnList;
    }

    public ArrayList<Task> getChildTasks(long task_id) {
        return getChildTasks(task_id, "1=1", null);
    }

    public ArrayList<Task> getChildTasks(long task_id, String where, String[] queryValues) {
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT "
                + "t1." + Task.TASK_ID + ", "
                + "t1." + Task.PARENT_ID + ", "
                + "t1." + Task.TASK_NAME + ", "
                + "t1." + Task.TASK_DESCRIPTION + ", "
                + "t1." + Task.TASK_COLOR + ", "
                + "t1." + Task.TASK_PROGRESS + ", "
                + "t1." + Task.TASK_IMPORTANCE + ", "
                + "t1." + Task.TASK_PRIORITY + ", "
                + "t1." + Task.TASK_TIME_ESTIMATE + ", "
                + "t1." + Task.TASK_TIME_ELAPSED + ", "
                + "t1." + Task.TASK_CREATED + ", "
                + "t1." + Task.TASK_LAST_MODIFIED + ", "
                + "t1." + Task.TASK_DEADLINE +
                " FROM " + Task.TABLE + " AS t1 " +
                " JOIN " + Task.TABLE + " AS t2 ON t2." + Task.TASK_ID + " = t1." + Task.PARENT_ID +
                " WHERE t2." + Task.TASK_ID + " = " + task_id + " AND " + where;
        ArrayList<Task> taskList = new ArrayList<Task>();

        Task task;
        Cursor cursor = db.rawQuery(selectQuery, queryValues);

        if (cursor.moveToFirst()) {
            do {
                task = new Task();

                task.setTaskId(cursor.getLong(cursor.getColumnIndex(Task.TASK_ID)));
                task.setParentId(cursor.getLong(cursor.getColumnIndex(Task.PARENT_ID)));
                task.setTaskName(cursor.getString(cursor.getColumnIndex(Task.TASK_NAME)));
                task.setDescription(cursor.getString(cursor.getColumnIndex(Task.TASK_DESCRIPTION)));
                task.setColor(cursor.getInt(cursor.getColumnIndex(Task.TASK_COLOR)));
                task.setProgress((byte) cursor.getInt(cursor.getColumnIndex(Task.TASK_PROGRESS)));
                task.setImportance(cursor.getInt(cursor.getColumnIndex(Task.TASK_IMPORTANCE)));
                task.setPriority(cursor.getInt(cursor.getColumnIndex(Task.TASK_PRIORITY)));
                task.setEstimate(cursor.getInt(cursor.getColumnIndex(Task.TASK_TIME_ESTIMATE)));
                task.setElapsed(cursor.getInt(cursor.getColumnIndex(Task.TASK_TIME_ELAPSED)));
                task.setCreated(new java.sql.Date(cursor.getLong(cursor.getColumnIndex(Task.TASK_CREATED))));
                task.setLastModified(new java.sql.Date(cursor.getLong(cursor.getColumnIndex(Task.TASK_LAST_MODIFIED))));
                task.setDeadline(new java.sql.Date(cursor.getLong(cursor.getColumnIndex(Task.TASK_DEADLINE))));

                taskList.add(task);
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return taskList;
    }

    public Task getTask(long task_id)
    {
        return getTask(Task.TASK_ID + "=?", new String[] { String.valueOf(task_id) });
    }

    public Task getTask(String where, String[] queryValues)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT "
                + Task.TASK_ID + ", "
                + Task.PARENT_ID + ", "
                + Task.TASK_NAME + ", "
                + Task.TASK_DESCRIPTION + ", "
                + Task.TASK_COLOR + ", "
                + Task.TASK_PROGRESS + ", "
                + Task.TASK_IMPORTANCE + ", "
                + Task.TASK_PRIORITY + ", "
                + Task.TASK_TIME_ESTIMATE + ", "
                + Task.TASK_TIME_ELAPSED + ", "
                + Task.TASK_CREATED + ", "
                + Task.TASK_LAST_MODIFIED + ", "
                + Task.TASK_DEADLINE +
                " FROM " + Task.TABLE +
                " WHERE " + where;

        Task task = new Task();
        Cursor cursor = db.rawQuery(selectQuery, queryValues);

        if (cursor.moveToFirst()) {
            do {
                task.setTaskId(cursor.getLong(cursor.getColumnIndex(Task.TASK_ID)));
                task.setParentId(cursor.getLong(cursor.getColumnIndex(Task.PARENT_ID)));
                task.setTaskName(cursor.getString(cursor.getColumnIndex(Task.TASK_NAME)));
                task.setDescription(cursor.getString(cursor.getColumnIndex(Task.TASK_DESCRIPTION)));
                task.setColor(cursor.getInt(cursor.getColumnIndex(Task.TASK_COLOR)));
                task.setProgress((byte) cursor.getInt(cursor.getColumnIndex(Task.TASK_PROGRESS)));
                task.setImportance(cursor.getInt(cursor.getColumnIndex(Task.TASK_IMPORTANCE)));
                task.setPriority(cursor.getInt(cursor.getColumnIndex(Task.TASK_PRIORITY)));
                task.setEstimate(cursor.getInt(cursor.getColumnIndex(Task.TASK_TIME_ESTIMATE)));
                task.setElapsed(cursor.getInt(cursor.getColumnIndex(Task.TASK_TIME_ELAPSED)));
                task.setCreated(new java.sql.Date(cursor.getLong(cursor.getColumnIndex(Task.TASK_CREATED))));
                task.setLastModified(new java.sql.Date(cursor.getLong(cursor.getColumnIndex(Task.TASK_LAST_MODIFIED))));
                task.setDeadline(new java.sql.Date(cursor.getLong(cursor.getColumnIndex(Task.TASK_DEADLINE))));

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return task;
    }

    public void recursiveDeleteTask(long task_id) {
        ArrayList<Task> childList = getChildTasks(task_id);

        for (Task t : childList)
            recursiveDeleteTask(t.getTaskId());

        deleteTask(task_id);
    }

    public void deleteTask(long task_id) {
        deleteTask(Task.TASK_ID + "=?", new String[] { String.valueOf(task_id) });
    }

    public void deleteTask(String where, String[] queryValues)
    {
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(Task.TABLE, where, queryValues);
        db.close();
    }

      ///////////////////////
     ////// Note CRUD //////
    ///////////////////////
    public long addNote(Note note)
    {
        ContentValues values = new ContentValues();
        values.put(Note.NOTE_NAME, note.getName());
        values.put(Note.NOTE_TEXT, note.getText());
        values.put(Note.NOTE_COLOR, note.getColor());
        values.put(Note.NOTE_CREATED, note.getCreated().getTime());
        values.put(Note.NOTE_LAST_MODIFIED, note.getLastModified().getTime());

        SQLiteDatabase db = this.getWritableDatabase();

        long note_id = db.insert(Note.TABLE, null, values);
        db.close();
        return note_id;
    }

    public void updateNote(Note note)
    {
        updateNote(note, Note.NOTE_ID + "=?", new String[] { String.valueOf(note.getNoteId()) });
    }

    public void updateNote(Note note, String where, String[] queryValues)
    {
        ContentValues values = new ContentValues();
        values.put(Note.NOTE_ID, note.getNoteId());
        values.put(Note.NOTE_NAME, note.getName());
        values.put(Note.NOTE_TEXT, note.getText());
        values.put(Note.NOTE_COLOR, note.getColor());
        values.put(Note.NOTE_CREATED, note.getCreated().getTime());
        values.put(Note.NOTE_LAST_MODIFIED, note.getLastModified().getTime());

        SQLiteDatabase db = this.getWritableDatabase();

        db.update(Note.TABLE, values, where, queryValues);
        db.close();
    }

    public ArrayList<Note> getNotes() {
        return getNotes("1=1", null);
    }

    public ArrayList<Note> getNotes(String where, String[] queryValues) {
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT "
                + Note.NOTE_ID + ", "
                + Note.NOTE_NAME + ", "
                + Note.NOTE_TEXT + ", "
                + Note.NOTE_COLOR + ", "
                + Note.NOTE_CREATED + ", "
                + Note.NOTE_LAST_MODIFIED +
                " FROM " + Note.TABLE +
                " WHERE " + where;
        ArrayList<Note> noteList = new ArrayList<Note>();

        Note note;
        Cursor cursor = db.rawQuery(selectQuery, queryValues);

        if (cursor.moveToFirst()) {
            do {
                note = new Note();

                note.setNoteId(cursor.getLong(cursor.getColumnIndex(Note.NOTE_ID)));
                note.setText(cursor.getString(cursor.getColumnIndex(Note.NOTE_NAME)));
                note.setText(cursor.getString(cursor.getColumnIndex(Note.NOTE_TEXT)));
                note.setColor(cursor.getInt(cursor.getColumnIndex(Note.NOTE_COLOR)));
                note.setCreated(new java.sql.Date(cursor.getLong(cursor.getColumnIndex(Note.NOTE_CREATED))));
                note.setLastModified(new java.sql.Date(cursor.getLong(cursor.getColumnIndex(Note.NOTE_LAST_MODIFIED))));

                noteList.add(note);
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return noteList;
    }

    public Note getNote(long note_id)
    {
        return getNote(Note.NOTE_ID + "=?", new String[] { String.valueOf(note_id) });
    }

    public Note getNote(String where, String[] queryValues)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT "
                + Note.NOTE_ID + ", "
                + Note.NOTE_NAME + ", "
                + Note.NOTE_TEXT + ", "
                + Note.NOTE_COLOR + ", "
                + Note.NOTE_CREATED + ", "
                + Note.NOTE_LAST_MODIFIED +
                " FROM " + Note.TABLE +
                " WHERE " + where;

        Note note = new Note();
        Cursor cursor = db.rawQuery(selectQuery, queryValues);

        if (cursor.moveToFirst()) {
            do {
                note.setNoteId(cursor.getLong(cursor.getColumnIndex(Note.NOTE_ID)));
                note.setText(cursor.getString(cursor.getColumnIndex(Note.NOTE_NAME)));
                note.setText(cursor.getString(cursor.getColumnIndex(Note.NOTE_TEXT)));
                note.setColor(cursor.getInt(cursor.getColumnIndex(Note.NOTE_COLOR)));
                note.setCreated(new java.sql.Date(cursor.getLong(cursor.getColumnIndex(Note.NOTE_CREATED))));
                note.setLastModified(new java.sql.Date(cursor.getLong(cursor.getColumnIndex(Note.NOTE_LAST_MODIFIED))));

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return note;
    }

    public void deleteNote(long note_id) {
        deleteNote(Note.NOTE_ID + "=?", new String[] { String.valueOf(note_id) });
    }

    public void deleteNote(String where, String[] queryValues)
    {
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(Note.TABLE, where, queryValues);
        db.close();
    }

      //////////////////////
     ////// Tag CRUD //////
    //////////////////////
    public long addTag(Tag tag)
    {
        ContentValues values = new ContentValues();
        values.put(Tag.TAG_NAME, tag.getTagName());
        values.put(Tag.TAG_COLOR, tag.getColor());

        SQLiteDatabase db = this.getWritableDatabase();

        long tag_id = db.insert(Tag.TABLE, null, values);
        db.close();
        return tag_id;
    }

    public void updateTag(Tag tag)
    {
        updateTag(tag, Tag.TAG_ID + "=?", new String[] { String.valueOf(tag.getTagId()) });
    }

    public void updateTag(Tag tag, String where, String[] queryValues)
    {
        ContentValues values = new ContentValues();
        values.put(Tag.TAG_ID, tag.getTagId());
        values.put(Tag.TAG_NAME, tag.getTagName());
        values.put(Tag.TAG_COLOR, tag.getColor());

        SQLiteDatabase db = this.getWritableDatabase();

        db.update(Tag.TABLE, values, where, queryValues);
        db.close();
    }

    public ArrayList<Tag> getTags() {
        return getTags("1=1", null);
    }

    public ArrayList<Tag> getTags(String where, String[] queryValues) {
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT "
                + Tag.TAG_ID + ", "
                + Tag.TAG_NAME + ", "
                + Tag.TAG_COLOR +
                " FROM " + Tag.TABLE +
                " WHERE " + where;
        ArrayList<Tag> tagList = new ArrayList<Tag>();

        Tag tag;
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                tag = new Tag();

                tag.setTagId(cursor.getLong(cursor.getColumnIndex(Tag.TAG_ID)));
                tag.setTagName(cursor.getString(cursor.getColumnIndex(Tag.TAG_NAME)));
                tag.setColor(cursor.getInt(cursor.getColumnIndex(Tag.TAG_COLOR)));

                tagList.add(tag);
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return tagList;
    }

    public Tag getTag(long tag_id)
    {
        return getTag(Tag.TAG_ID + "=?", new String[] { String.valueOf(tag_id) });
    }

    public Tag getTag(String where, String[] queryValues)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT "
                + Tag.TAG_ID + ", "
                + Tag.TAG_NAME + ", "
                + Tag.TAG_COLOR +
                " FROM " + Tag.TABLE +
                " WHERE " + where;

        Tag tag = new Tag();
        Cursor cursor = db.rawQuery(selectQuery, queryValues);

        if (cursor.moveToFirst()) {
            do {
                tag.setTagId(cursor.getLong(cursor.getColumnIndex(Tag.TAG_ID)));
                tag.setTagName(cursor.getString(cursor.getColumnIndex(Tag.TAG_NAME)));
                tag.setColor(cursor.getInt(cursor.getColumnIndex(Tag.TAG_COLOR)));

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return tag;
    }

    public void deleteTag(long tag_id) {
        deleteTask(Tag.TAG_ID + "=?", new String[] { String.valueOf(tag_id) });
    }

    public void deleteTag(String where, String[] queryValues)
    {
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(Tag.TABLE, where, queryValues);
        db.close();
    }

      ////////////////////////////
     // Task-Note Mapping CRUD //
    ////////////////////////////
    public long addTaskNoteMapping(TaskNoteMapping tnm)
    {
      ContentValues values = new ContentValues();
      values.put(TaskNoteMapping.TASK_ID, tnm.getTaskId());
      values.put(TaskNoteMapping.NOTE_ID, tnm.getNoteId());

      SQLiteDatabase db = this.getWritableDatabase();

      long tnm_id = db.insert(TaskNoteMapping.TABLE, null, values);
      db.close();
      return tnm_id;
    }

    public ArrayList<Note> getTaskNotes(long task_id) {
        return getTaskNotes(task_id, "1=1", null);
    }

    public ArrayList<Note> getTaskNotes(long task_id, String where, String[] queryValues) {
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT "
                + "n." + Note.NOTE_ID + ", "
                + "n." + Note.NOTE_NAME + ", "
                + "n." + Note.NOTE_TEXT + ", "
                + "n." + Note.NOTE_COLOR + ", "
                + "n." + Note.NOTE_CREATED + ", "
                + "n." + Note.NOTE_LAST_MODIFIED +
                " FROM " + Note.TABLE + " AS n" +
                " JOIN " + TaskNoteMapping.TABLE + " AS m ON m." + TaskNoteMapping.NOTE_ID + " = n." + Note.NOTE_ID +
                " JOIN " + Task.TABLE + " AS t ON t." + Task.TASK_ID + " = " + task_id +
                " WHERE " + where;
        ArrayList<Note> noteList = new ArrayList<Note>();

        Note note;
        Cursor cursor = db.rawQuery(selectQuery, queryValues);

        if (cursor.moveToFirst()) {
            do {
                note = new Note();

                note.setNoteId(cursor.getLong(cursor.getColumnIndex(Note.NOTE_ID)));
                note.setText(cursor.getString(cursor.getColumnIndex(Note.NOTE_NAME)));
                note.setText(cursor.getString(cursor.getColumnIndex(Note.NOTE_TEXT)));
                note.setColor(cursor.getInt(cursor.getColumnIndex(Note.NOTE_COLOR)));
                note.setCreated(new java.sql.Date(cursor.getLong(cursor.getColumnIndex(Note.NOTE_CREATED))));
                note.setLastModified(new java.sql.Date(cursor.getLong(cursor.getColumnIndex(Note.NOTE_LAST_MODIFIED))));

                noteList.add(note);
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return noteList;
    }

      ///////////////////////////
     // Task-Tag Mapping CRUD //
    ///////////////////////////
    public long addTaskTagMapping(TaskTagMapping ttm)
    {
      ContentValues values = new ContentValues();
      values.put(TaskTagMapping.TASK_ID, ttm.getTaskId());
      values.put(TaskTagMapping.TAG_ID, ttm.getTagId());

      SQLiteDatabase db = this.getWritableDatabase();

      long ttm_id = db.insert(TaskTagMapping.TABLE, null, values);
      db.close();
      return ttm_id;
    }

    public ArrayList<Task> getTagTasks(long tag_id) {
        return getTagTasks(tag_id, "1=1", null);
    }

    public ArrayList<Task> getTagTasks(long tag_id, String where, String[] queryValues) {
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT "
                + Task.TASK_ID + ", "
                + Task.PARENT_ID + ", "
                + Task.TASK_NAME + ", "
                + Task.TASK_DESCRIPTION + ", "
                + Task.TASK_COLOR + ", "
                + Task.TASK_PROGRESS + ", "
                + Task.TASK_IMPORTANCE + ", "
                + Task.TASK_PRIORITY + ", "
                + Task.TASK_TIME_ESTIMATE + ", "
                + Task.TASK_TIME_ELAPSED + ", "
                + Task.TASK_CREATED + ", "
                + Task.TASK_LAST_MODIFIED + ", "
                + Task.TASK_DEADLINE +
                " FROM " + Task.TABLE +
                " JOIN " + TaskTagMapping.TABLE + " ON " + TaskTagMapping.TASK_ID + " = " + Task.TASK_ID +
                " JOIN " + Tag.TABLE + " ON " + Tag.TAG_ID + " = " + tag_id +
                " WHERE " + where;
        ArrayList<Task> taskList = new ArrayList<Task>();

        Task task;
        Cursor cursor = db.rawQuery(selectQuery, queryValues);

        if (cursor.moveToFirst()) {
            do {
                task = new Task();

                task.setTaskId(cursor.getLong(cursor.getColumnIndex(Task.TASK_ID)));
                task.setParentId(cursor.getLong(cursor.getColumnIndex(Task.PARENT_ID)));
                task.setTaskName(cursor.getString(cursor.getColumnIndex(Task.TASK_NAME)));
                task.setDescription(cursor.getString(cursor.getColumnIndex(Task.TASK_DESCRIPTION)));
                task.setColor(cursor.getInt(cursor.getColumnIndex(Task.TASK_COLOR)));
                task.setProgress((byte) cursor.getInt(cursor.getColumnIndex(Task.TASK_PROGRESS)));
                task.setImportance(cursor.getInt(cursor.getColumnIndex(Task.TASK_IMPORTANCE)));
                task.setPriority(cursor.getInt(cursor.getColumnIndex(Task.TASK_PRIORITY)));
                task.setEstimate(cursor.getInt(cursor.getColumnIndex(Task.TASK_TIME_ESTIMATE)));
                task.setElapsed(cursor.getInt(cursor.getColumnIndex(Task.TASK_TIME_ELAPSED)));
                task.setCreated(new java.sql.Date(cursor.getLong(cursor.getColumnIndex(Task.TASK_CREATED))));
                task.setLastModified(new java.sql.Date(cursor.getLong(cursor.getColumnIndex(Task.TASK_LAST_MODIFIED))));
                task.setDeadline(new java.sql.Date(cursor.getLong(cursor.getColumnIndex(Task.TASK_DEADLINE))));

                taskList.add(task);
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return taskList;
    }
}