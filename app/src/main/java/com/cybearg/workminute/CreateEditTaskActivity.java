package com.cybearg.workminute;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;

import java.sql.Date;
import java.util.Calendar;


public class CreateEditTaskActivity extends ActionBarActivity {

    private long _taskID;
    private long _parentID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_edit_task);
        Intent intent = getIntent();
        _taskID = intent.getLongExtra(TaskListActivity.TASK_ID, -1);
        _parentID = intent.getLongExtra(TaskListActivity.PARENT_ID, -1);

        if (_taskID == -1) {// if creating new task...
            Button addEditButton = (Button) findViewById(R.id.btnAddEditTask);
            Button deleteButton = (Button) findViewById(R.id.btnDeleteTask);
            addEditButton.setText("Add");
            setTitle("Add Task");
            deleteButton.setEnabled(false);
        } else { // if editing existing task...
            Button addEditButton = (Button) findViewById(R.id.btnAddEditTask);
            addEditButton.setText("Save");
            DBHelper dbHelper = new DBHelper(ApplicationContextProvider.getContext());
            Task task = dbHelper.getTask(_taskID);
            setTitle("Edit Task " + task.getTaskName());

            EditText txtTaskName = (EditText) findViewById(R.id.etTaskName);
            EditText txtTaskDesc = (EditText) findViewById(R.id.etTaskDesc);
            Spinner spnTaskImportance = (Spinner) findViewById(R.id.spnTaskImportance);
            Spinner spnTaskPriority = (Spinner) findViewById(R.id.spnTaskPriority);
           // DatePicker dpTaskDeadline = (DatePicker) findViewById(R.id.dpTaskDeadline);

            txtTaskName.setText(task.getTaskName());
            txtTaskDesc.setText(task.getDescription());
            spnTaskImportance.setSelection(task.getImportance());
            spnTaskPriority.setSelection(task.getPriority());
            //dpTaskDeadline.set
        }
    }

    public void btnAddEditTask_Click(View view) {

        DBHelper dbHelper = new DBHelper(ApplicationContextProvider.getContext());
        Task task;

        if (_taskID == -1) // new task
            task = new Task();
        else
            task = dbHelper.getTask(_taskID);

        EditText txtTaskName = (EditText) findViewById(R.id.etTaskName);
        EditText txtTaskDesc = (EditText) findViewById(R.id.etTaskDesc);
        Spinner spnTaskImportance = (Spinner) findViewById(R.id.spnTaskImportance);
        Spinner spnTaskPriority = (Spinner) findViewById(R.id.spnTaskPriority);
        //DatePicker dpTaskDeadline = (DatePicker) findViewById(R.id.dpTaskDeadline);

        Calendar cal = Calendar.getInstance();
        java.util.Date currDate = cal.getTime();

        task.setTaskName(txtTaskName.getText().toString());
        task.setDescription(txtTaskDesc.getText().toString());
        task.setImportance(spnTaskImportance.getSelectedItemPosition());
        task.setPriority(spnTaskPriority.getSelectedItemPosition());
        task.setLastModified(new Date(currDate.getTime()));

        if (_taskID == -1) { // new task
            task.setParentId(_parentID);
            _taskID = dbHelper.addTask(task);
        }
        else
            dbHelper.updateTask(task);

        returnToTask(RESULT_OK);
    }

    public void btnDeleteTask_Click(View view) {
        DBHelper dbHelper = new DBHelper(ApplicationContextProvider.getContext());
        dbHelper.recursiveDeleteTask(_taskID);
        returnToTask(-2);
    }

    public void btnCancelTask_Click(View view) {
        returnToTask(RESULT_OK);
    }

    private void returnToTask(int result) {
        Intent data = new Intent();
        data.putExtra("taskID", _taskID);
        setResult(result, data);
        this.finish();
    }


/*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_create_edit_task, menu);
        return true;
    }
*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
