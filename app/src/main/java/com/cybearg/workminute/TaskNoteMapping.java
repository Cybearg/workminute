package com.cybearg.workminute;

/**
 * Created by Cybearg on 4/9/2015.
 */
public class TaskNoteMapping {

    // Table name
    public static final String TABLE = "task_note_mapping";

    // Column names
    public static final String TASK_ID = "task_id";
    public static final String NOTE_ID = "note_id";

    private long _task_id, _note_id;

    TaskNoteMapping(int task_id, int note_id)
    {
        this._task_id = task_id;
        this._note_id = note_id;
    }

    public long getTaskId() {
        return _task_id;
    }

    public void setTaskId(long _task_id) {
        this._task_id = _task_id;
    }

    public long getNoteId() {
        return _note_id;
    }

    public void setNoteId(long _note_id) {
        this._note_id = _note_id;
    }
}
