package com.cybearg.workminute;

import android.content.Context;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;


public class MainActivity extends ActionBarActivity{

    private enum activity_type {ADD, EDIT}

    private activity_type activity;
    private Context sContext;
    Button btnAdd;
    ListView lvNotes;
    EditText txtColor, txtText;
    long selectedNoteID;

    private void setActivity(activity_type aType) {
        activity = aType;

        if (aType == activity_type.ADD) {
            btnAdd.setText(getString(R.string.btnAdd_add));

            txtColor.setText("");
            txtText.setText("");
        } else if (aType == activity_type.EDIT) {
            btnAdd.setText(getString(R.string.btnAdd_edit));

            DBHelper dbHelper = new DBHelper(this);
            Note note = dbHelper.getNote(selectedNoteID);

            txtColor.setText(Integer.toString(note.getColor()));
            txtText.setText(note.getText());
        }
    }

    protected void updateNoteList() {
        DBHelper dbHelper = new DBHelper(this);
        ArrayList<Note> noteArray = dbHelper.getNotes();
        NoteAdapter notesAdapter = new NoteAdapter(this, noteArray);
        lvNotes.setAdapter(notesAdapter);
    }

    public void btnAdd_onClick(View view) {
        if (!txtColor.getText().toString().trim().equals("")
                && !txtText.getText().toString().trim().equals("")) {
            if (activity == activity_type.ADD) {
                Note note = new Note();
                note.setColor(Integer.parseInt(txtColor.getText().toString()));
                note.setText(txtText.getText().toString());

                DBHelper dbHelper = new DBHelper(this);
                dbHelper.addNote(note);

                updateNoteList();
            } else if (activity == activity_type.EDIT) {
                DBHelper dbHelper = new DBHelper(this);
                Note note = dbHelper.getNote(selectedNoteID);

                note.setColor(Integer.parseInt(txtColor.getText().toString()));
                note.setText(txtText.getText().toString());

                dbHelper.updateNote(note);

                updateNoteList();
            }

            setActivity(activity_type.ADD);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sContext = this;

        btnAdd = (Button) findViewById(R.id.btnAdd);
        lvNotes = (ListView) findViewById(R.id.lvNotes);
        txtColor = (EditText) findViewById(R.id.txtColor);
        txtText = (EditText) findViewById(R.id.txtText);

        setActivity(activity_type.ADD);

        lvNotes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String noteID = ((TextView)view.findViewById(R.id.noteID)).getText().toString();
                long oldNoteID = selectedNoteID;
                selectedNoteID = Long.valueOf(noteID).longValue();

                if (activity == activity_type.ADD || oldNoteID != selectedNoteID) {
                    setActivity(activity_type.EDIT);
                }
                else if (oldNoteID == selectedNoteID) {
                    DBHelper dbHelper = new DBHelper(sContext);
                    dbHelper.deleteNote(selectedNoteID);
                    updateNoteList();

                    setActivity(activity_type.ADD);
                }
            }
        });

        updateNoteList();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
