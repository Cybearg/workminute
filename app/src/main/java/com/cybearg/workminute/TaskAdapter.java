package com.cybearg.workminute;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.security.KeyPair;
import java.util.AbstractMap;
import java.util.ArrayList;

/**
 * Created by Cybearg on 4/21/2015.
 */
public class TaskAdapter extends ArrayAdapter<Task> {
    private ArrayList<Task> tasks;

    public TaskAdapter(Context context, ArrayList<Task> tasks) {
        super(context, 0, tasks);
        this.tasks = tasks;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Task task = tasks.get(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_task, parent, false);
        }

        if (task != null) {
            TextView taskID = (TextView) convertView.findViewById(R.id.taskID);
            TextView taskName = (TextView) convertView.findViewById(R.id.taskName);
            TextView taskDesc = (TextView) convertView.findViewById(R.id.taskDesc);
            ProgressBar taskProg = (ProgressBar) convertView.findViewById(R.id.taskProgress);
            TextView taskNotes = (TextView) convertView.findViewById(R.id.taskNotes);
            TextView taskChildren = (TextView) convertView.findViewById(R.id.taskChildren);
            TextView taskPriority = (TextView) convertView.findViewById(R.id.taskPriority);
            TextView taskImportance = (TextView) convertView.findViewById(R.id.taskImportance);
            int noteCount;
            int childCount;

            taskID.setText(Long.toString(task.getTaskId()));
            taskName.setText(task.getTaskName());
            taskDesc.setText(task.getDescription());
            taskProg.setProgress(task.getProgress());

            // Run queries to get counts of notes and children
            DBHelper dbHelper = new DBHelper(getContext());
            noteCount = dbHelper.getTaskNotes(task.getTaskId()).size();
            childCount = dbHelper.recursiveGetChildTasks(task.getTaskId()).size();

            taskNotes.setText(Integer.toString(noteCount) + " " + getContext().getString(R.string.task_notecount_label));
            taskNotes.setVisibility(View.GONE);
            taskChildren.setText(Integer.toString(childCount) + " " + getContext().getString(R.string.task_childcount_label));

            // For assigning colors/names to priority and importance text
            AbstractMap.SimpleEntry<Integer, String> colorNamePair;

            colorNamePair = findUrgency(task.getPriority());
            taskPriority.setText(colorNamePair.getValue());
            taskPriority.setTextColor(colorNamePair.getKey());

            colorNamePair = findUrgency(task.getImportance());
            taskImportance.setText(colorNamePair.getValue());
            taskImportance.setTextColor(colorNamePair.getKey());
        }

        return convertView;
    }

    public static AbstractMap.SimpleEntry<Integer, String> findUrgency(int priority) {
        Integer color;
        String name;

        switch (priority) {
            case 1: color = Color.BLUE; name = "Low"; break;
            case 2: color = Color.YELLOW; name = "Medium"; break;
            case 3: color = Color.parseColor("#FF8400"); name = "High"; break;
            case 4: color = Color.RED; name = "Critical"; break;
            default: color = Color.BLACK; name = "None"; break;
        }

        return new AbstractMap.SimpleEntry<>(color, name);
    }
}
