package com.cybearg.workminute;

import java.sql.Date;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by Cybearg on 4/7/2015.
 */
public class Task {
    // Table name
    public static final String TABLE = "tasks";

    // Column names
    public static final String TASK_ID = "task_id";
    public static final String PARENT_ID = "task_parent_id";
    public static final String TASK_NAME = "task_name";
    public static final String TASK_DESCRIPTION = "task_description";
    public static final String TASK_COLOR = "task_color";
    public static final String TASK_PROGRESS = "task_progress";
    public static final String TASK_IMPORTANCE = "task_importance";
    public static final String TASK_PRIORITY = "task_priority";
    public static final String TASK_TIME_ESTIMATE = "task_time_estimate";
    public static final String TASK_TIME_ELAPSED = "task_time_elapsed";
    public static final String TASK_CREATED = "task_created";
    public static final String TASK_LAST_MODIFIED = "task_last_modified";
    public static final String TASK_DEADLINE = "task_deadline";

    private long _task_id, _parent;
    private int _color, _importance, _priority, _estimate, _elapsed;
    private byte _progress;
    private String _name, _description;
    private Date _created, _last_modified, _deadline;

    public Task()
    {
        this._task_id = 0;
        this._parent = -1;
        this._name = "";
        this._description = "";
        this._color = 0;
        this._progress = 0;
        this._importance = 0;
        this._priority = 0;
        this._estimate = 0;
        this._elapsed = 0;
        Calendar cal = Calendar.getInstance();
        java.util.Date currDate = cal.getTime();
        this._created = new java.sql.Date(currDate.getTime());
        this._last_modified = new java.sql.Date(currDate.getTime());
        this._deadline = new java.sql.Date(currDate.getTime());
    }

    public Task (int id, int parent_id, int tag_id, String name, String description,
                 int color, byte progress, int importance, int priority,
                 int time_estimate, int time_elapsed, Date created, Date last_modified, Date deadline) {
        this._task_id = id;
        this._parent = parent_id;
        this._name = name;
        this._description = description;
        this._color = color;
        this._progress = progress;
        this._importance = importance;
        this._priority = priority;
        this._estimate = time_estimate;
        this._elapsed = time_elapsed;
        this._created = created;
        this._last_modified = last_modified;
        this._deadline = deadline;
    }

    public long getTaskId() {
        return _task_id;
    }

    public void setTaskId(long _task_id) {
        this._task_id = _task_id;
    }

    public long getParentId() {
        return _parent;
    }

    public void setParentId(long _parent) {
        this._parent = _parent;
    }

    public int getColor() {
        return _color;
    }

    public void setColor(int _color) {
        this._color = _color;
    }

    public int getImportance() {
        return _importance;
    }

    public void setImportance(int _importance) {
        this._importance = _importance;
    }

    public int getPriority() {
        return _priority;
    }

    public void setPriority(int _priority) {
        this._priority = _priority;
    }

    public int getEstimate() {
        return _estimate;
    }

    public void setEstimate(int _estimate) {
        this._estimate = _estimate;
    }

    public int getElapsed() {
        return _elapsed;
    }

    public void setElapsed(int _elapsed) {
        this._elapsed = _elapsed;
    }

    public byte getProgress() {
        return _progress;
    }

    public void setProgress(byte _progress) {
        this._progress = _progress;
    }

    public String getTaskName() {
        return _name;
    }

    public void setTaskName(String _name) {
        this._name = _name;
    }

    public String getDescription() {
        return _description;
    }

    public void setDescription(String _description) {
        this._description = _description;
    }

    public Date getCreated() {
        return _created;
    }

    public void setCreated(Date _created) {
        this._created = _created;
    }

    public Date getLastModified() {
        return _last_modified;
    }

    public void setLastModified(Date _last_modified) {
        this._last_modified = _last_modified;
    }

    public Date getDeadline() {
        return _deadline;
    }

    public void setDeadline(Date _deadline) {
        this._deadline = _deadline;
    }
}
