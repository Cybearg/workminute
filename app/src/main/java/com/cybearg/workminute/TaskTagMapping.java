package com.cybearg.workminute;

/**
 * Created by Cybearg on 4/13/2015.
 */
public class TaskTagMapping {

    // Table name
    public static final String TABLE = "task_tag_mapping";

    // Column names
    public static final String TASK_ID = "task_id";
    public static final String TAG_ID = "tag_id";

    private long _task_id, _tag_id;

    TaskTagMapping(int task_id, int tag_id)
    {
        this._task_id = task_id;
        this._tag_id = tag_id;
    }

    public long getTaskId() {
        return _task_id;
    }

    public void setTaskId(long task_id) {
        this._task_id = task_id;
    }

    public long getTagId() {
        return _tag_id;
    }

    public void setTagId(long tag_id) {
        this._tag_id = tag_id;
    }
}
